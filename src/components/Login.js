import React, { useState } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import '../assets/css/logSign.css'
function Login({setData,data,acc,setAcc}) {
    const navigate=useNavigate('')
    const [em,setEm] = useState('')
    const [pass,setPass] = useState('')
    function handleSubmit(e){
        e.preventDefault()
        
        const updatedData = [...data].map((item)=>{
          if(item.e_mail===em){
            if(item.password===pass){
              item.logged_in = true
              setAcc(item)
            }
          }
          return item
        })
        setData(updatedData)
        navigate('../home')
    }
  return (
    <>
    <form className="form-class" onSubmit={(e)=>handleSubmit(e)}>
       <input className="log-signup" type="email" placeholder='email' onChange={(e)=>setEm(e.target.value)} required></input>
        <input className="log-signup" type="password" placeholder='create password' onChange={(e)=>setPass(e.target.value)} required></input>
       <input className="log-signup" type="submit" />
       <Link to='/SignUp'>If already not a member</Link>
    </form>
    
    </>
  )
}

export default Login