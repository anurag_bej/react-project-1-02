import React,{useState, useCallback} from 'react';
import Todos from './Todos.js'
import '../App.css'
import Navbar from './Nav-bar.js';
function UseCallBack() {
  const[todo, setTodo] = useState([])
  const [count, setcount] = useState(0)
  const handlecallback = useCallback(
    () => {
      console.log("Some Operation")
    },
    [todo],
  )  
  return (
    <>
    <Navbar/>
    <div className="xyz">
      <Todos todo={todo} callback={handlecallback}/>
      <button onClick={()=>setcount(count+1)}>increment</button>
      <div>
        value is {count}
      </div>
    </div>
    </>
  );
}

export default UseCallBack;
