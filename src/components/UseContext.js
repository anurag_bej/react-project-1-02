import '../App.css';
import A from './A';
import Navbar from './Nav-bar';
function UseContext() {
  return (
    <>
    <Navbar/>
    <div className="App xyz">
        <A/>
    </div>
    </>
  );
}
export default UseContext;