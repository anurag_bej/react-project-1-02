import React  from 'react'
 function Cards({data ,card , setcard , toggleButton ,setData }) {
    
    function changeColor(id){
      const updatedData = [...data].map((item)=>{
        if(item.id === id){
          item.toggle = ! item.toggle
          if(item.toggle === false){
          setcard('')
        }
        }
        else{
          item.toggle = false

        }
       
        return item;
      })
      setData(updatedData)
    }
      
  return (
       <>
      <div>
        {data.map((item)=>{
            return <div className='card' style={item.toggle ? {backgroundColor:'blue'}: {backgroundColor:'white'}}  onClick= { ()=> { toggleButton (); setcard(item.title); changeColor(item.id); } }>
              {item.title} </div>
        })}
        </div>
      <h5>
        {card}
      </h5>
      </>
  )
}
export default Cards