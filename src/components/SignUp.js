import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import '../assets/css/logSign.css'
function SignUp({setData,data}) {
    const navigate = useNavigate('/Login')
    const [fname,setFname] = useState('')
    const [lname,setLname] = useState('')
    const [em,setEm] = useState('')
    const [pass,setPass] = useState('')
    const [suid,setSuid] = useState(0)
    const [message,setMessage] = useState('')
    // function emailValidation(email){
     
    //   const regex = /[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,8}(.[a-z{2,8}])?/g
    //   if(regex.test(email)){
    //     setMessage("Email is valid")
    //   }else if(!regex.test(email)&&email!==""){
    //     setMessage("Email is not valid")
    //   }
    // }
    function handleSubmit(e){
        e.preventDefault()
        
        setSuid(prevState=>{return prevState+1})
        let formData = {
            id:suid,
            first_name : fname,
            last_name : lname,
            e_mail : em,
            password : pass,
            logged_in : false,
        }
        // emailValidation(em)
        if(data.find((item)=>item.e_mail===em)){
          console.log('Email ID already taken')
          navigate('/')
        }
        setData([...data].concat(formData))
        formData = {}
        navigate('/')
    }
  return (
    <>
    <form onSubmit={handleSubmit}>
        <input className="log-signup" type="text" placeholder='firstname' onChange={(e)=>setFname(e.target.value)} required></input><br></br>
        <input className="log-signup" type="text" placeholder='lastname' onChange={(e)=>setLname(e.target.value)} required></input><br></br>
        <input className="log-signup" type="email" placeholder='email' onChange={(e)=>setEm(e.target.value)} required></input><br></br>
        <input className="log-signup" type="password" placeholder='create password' onChange={(e)=>setPass(e.target.value)} required></input><br></br>
       <input  className="log-signup" type="password" placeholder='retype password' onChange={(e)=>setPass(e.target.value)} required></input><br></br>
        <input className="log-signup" type="submit" />
        {message}
    </form>

    </>
  )
}

export default SignUp