import React, { useEffect } from 'react'
import '../App.css'
import Navbar from './Nav-bar'
function B() {
    const [todos, setTodos] = React.useState([])
    const [todo, setTodo] = React.useState("")
    const [todoEditing, setTodoEditing] = React.useState(null)
    const [editingText, setEditingText] = React.useState('')
    useEffect(() => {
        const temp = JSON.stringify(todos)
        localStorage.setItem('todos', temp)
    }, [todos])

    function handleSubmit(e) {
        e.preventDefault()
        const newTodo = {
            id: new Date().getTime(),
            text: todo,
            completed: false
        }
        setTodos([...todos].concat(newTodo))
        setTodo('')
    }
    function deleteTodo(id) {
        const updatedTodo = [...todos].filter((todo) => todo.id !== id)
        setTodos(updatedTodo)
    }
    function toggleComplete(id) {
        const updatedTodos = [...todos].map((todo) => {
            if (todo.id === id) {
                todo.completed = !todo.completed
            }
            return todo
        })
        setTodos(updatedTodos)
    }
    function editTodo(id) {
        const updatedTodos = [...todos].map((todo) => {
            if (todo.id === id) {
                todo.text = editingText
            }
            return todo
        })
        setTodos(updatedTodos)
        setTodoEditing(null)
        setEditingText('')
    }
    return (
        <>
            <Navbar />
            <div className='xyz'>
                <form onSubmit={handleSubmit}>
                    <input type='text' onChange={(e) => setTodo(e.target.value)} value={todo} />
                    <button type='submit'>ADD</button>
                </form>
                {todos.map((todo) => <div key={todo.id}>
                    {todoEditing === todo.id ?
                        (<input type='text'
                            onChange={(e) => setEditingText(e.target.value)}
                            value={editingText} />) :
                        (<div>
                            {todo.text}
                        </div>)}
                    <button onClick={() => deleteTodo(todo.id)}> DELETE</button>
                    <input
                        type='checkbox' onChange={() => toggleComplete(todo.id)}
                        checked={todo.completed} />
                    {todoEditing === todo.id ? (<button onClick={() => editTodo(todo.id)}>Submit</button>) :
                        (<button onClick={() => setTodoEditing(todo.id)}> EDIT </button>)}
                </div>)}
            </div>
        </>
    )
}
export default B;
