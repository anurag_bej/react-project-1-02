import React from 'react';
import {  Link } from "react-router-dom";
import '../assets/css/NavBar.css'
import { useState } from 'react'; 
const Navbar= () =>{
  const [toggle, setToggle] = useState(false)
  const [hookToggle, setHookToggle] = useState(false)
  const [data,setData] = useState([
    {
      id:1,
      component_name:'TodosList',
      isLink:true
    },
    {
      id:2,
      component_name:'CardList',
      isLink:true
    },{
      id:3,
      component_name:'Hooks',
      isLink:false
    },
    {
      id:4,
      component_name:'Home',
      isLink:true
    },
    /*
    {
      id:5,
      component_name:'SignUp',
      isLink:true
    },
    {
      id:6,
      component_name:'Login',
      isLink:true
    }
    */
  ])
const[hooks, setHooks] = useState([
  {
    id:11,
    component_name : "useMemo",
    toggle:false
  },
  {
    id:12,
    component_name : "useCallBack",
    toggle:false
  },
  {
    id:13,
    component_name : "useContext",
    toggle:false
  },
])

  return (
    <>
    <div id="navigation">
            <div id="menu" className={hookToggle?'':'icon'} onClick={()=>{setToggle(!toggle);}}>
                <div id="bar1" className="bar"></div>
                <div id="bar2" className="bar"></div>
                <div id="bar3" className="bar"> </div>
            </div>
</div>

{/*
<div>
<ul  id="dog-names" style={hookToggle? {}:{display: 'none'}}>
  <li>Rigatoni     </li>
  <li >Dave        </li>
  <li >Pumpernickel</li>
  <li >Reeses      </li>
</ul>

</div>
  */}
        <div>
        <ul className={toggle?"change":"nav"} id="nav" style={toggle? {}:{display: 'none'}}  >
            {
              data.map((item)=>{
                if(item.isLink){
                  return <li key={item.id}><Link to={`/${item.component_name}`}>{item.component_name}</Link></li>

                }else{
                  return <><li style={{cursor:'pointer'}} onClick={()=>setHookToggle(!hookToggle)} key={item.id}>{item.component_name}</li>
                  {hooks.map((it)=>{
                    return <li key={it.id}style={hookToggle? {}:{display: 'none'}}><Link to={`/${it.component_name}`}>{it.component_name}</Link></li>
                  })}
                  </>
                }
                
              
              })
            }
        
            </ul>
        </div>
        </>
  )
}      

export default Navbar;
