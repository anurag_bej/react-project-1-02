import React,{createContext} from 'react'
import B from './B'
const FirstName = createContext()
const LastName = createContext()
 function A() {
  return (
    <div>
      <FirstName.Provider value='Anurag'>
        <LastName.Provider value='Bej'>
      <B/>
      </LastName.Provider>
      </FirstName.Provider>
    </div>
  )
}
export default A
export {FirstName,LastName}