import '../App.css';
import { useState, useMemo } from 'react'
import Navbar from './Nav-bar';
function UseMemo() {
  const [countOne, setCountOne] = useState(0)
  const [countTwo, setCountTwo] = useState(0)

  function increment() {
    setCountOne(countOne + 1);
  }
  function decrement() {
    setCountTwo(countTwo - 1);
  }
  const isEven = useMemo(() => {
    for (let i = 0; i <= 1000000000; i++) {
    }
    return countOne % 2 === 0
  }, [countOne])

  return (
    <>
      <Navbar/>
      <div className="App xyz">

        <button onClick={increment}> CountOne{countOne}</button>
        <button onClick={decrement}>CountTwo {countTwo} </button>
        {isEven ? <p>Even</p> : <p>Odd</p>}
      </div>
    </>
  );
}

export default UseMemo;
