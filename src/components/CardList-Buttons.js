import React from 'react'
 function Buttons({ data ,card , setcard , toggleButton ,setData }) {
  return (
    <>
       <div className='greetings'>
      hello {card}
      </div>
      <button onClick={(()=>{
        const updatedData = [...data].map((item)=>{
          item.toggle = false
          setcard ('')
          toggleButton('')
          return item
        })
        setData(updatedData)
      })}>
        Clear
      </button>
    </>
  )
}
export default Buttons