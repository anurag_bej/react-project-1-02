import React from 'react'
import '../App.css'
import Buttons from './CardList-Buttons'
import Cards from './CardList-Cards.js'
import { useState } from 'react'
import '../assets/css/CardList.css'
import Navbar from './Nav-bar'
export default function C() {
   
        const [data,setData] = useState([ {
            id:1,
            toggle:false,
            title : "1"
          },
          {
            id:2,
            toggle:false,
            title: "2"
          },
          {
            id:3,
            toggle:false,
            title: "3"
          },
        ])
        const [card, setcard] = useState('')
      const [toggle, setToggle] = useState('');
      const toggleButton = () => setToggle(!toggle)
  return (
    <>
      <Navbar/>
    <div className='xyz'>
      <Cards  data = {data} card ={card} setcard = {setcard} toggleButton ={toggleButton} setData = {setData}/>
      <Buttons data = {data} card ={card} setcard = {setcard} toggleButton ={toggleButton} setData = {setData}/>
    </div>
    </>
  )
}

