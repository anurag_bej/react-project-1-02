import './App.css'
import TodoList from './components/TodosList'
import CardList from './components/CardList'
import {Route, Routes } from "react-router-dom";
import UseCallBack from './components/UseCallBack'

import UseMemo from './components/UseMemo';
import UseContext from './components/UseContext';
import Home from './components/Home';
import SignUp from './components/SignUp';
import Login from './components/Login';
import { useState } from 'react';
function App() {
  const [data,setData] = useState([])
  const [acc,setAcc] = useState([])
  return (
    <>
    <div className="App">
      <Routes>
          <Route path='/TodosList' exact element={<TodoList/>} />
        <Route path='/CardList' exact element={<CardList/>} />
        <Route exact path='/useCallBack'  element={<UseCallBack/>}/>
        <Route path='/useMemo' exact element={<UseMemo/>}/>
        <Route path='/useContext' exact element={<UseContext/>}/>
        <Route path='/Hooks' exact element={<TodoList/>}/>

        <Route exact path="/" element={<Login data={data} setData={setData} acc={acc} setAcc={setAcc}/>}></Route>
        <Route exact path="/SignUp" element={<SignUp data={data} setData={setData} acc={acc} setAcc={setAcc}/>}></Route>
        <Route exact path="/Home" element={<Home acc={acc} setAcc={setAcc}/>}></Route>
        <Route/> 
        </Routes>
    </div>
    </>
  );
}

export default App;
